package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Random;

public class cust_algorithm {

    // Simulator configurations
    public int customercount = 200000;
    public int customerorder;
    public int customerordertime;
    public int totalweight;
    public int i = 0;
    Random rand = new Random();

    public int customernumber = 0;

    // Connection Variables
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/simulator?rewriteBatchedStatements=true";
    static final String USER = "root";
    static final String PASS = "ve22soner";
    static Connection conn = null;
    static java.sql.Statement stmt = null;

    public cust_algorithm() {

        try {
            customerdivisioncreator();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        cust_algorithm a = new cust_algorithm();
    }

    private void customerdivisioncreator() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");

            stmt = conn.createStatement();
            String sql;
            sql = "select * from customer_division_conf";
            java.sql.ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int Hour = rs.getInt("hour");
                int Weight_min = rs.getInt("weight_min");
                int Weight_max = rs.getInt("weight_max");

                int randcustomerweight = rand.nextInt((Weight_max - Weight_min) + 1) + (Weight_min - 1);

                System.out.println(Hour + " " + Weight_min + " " + Weight_max + " weight = " + randcustomerweight);

                java.sql.Statement stmt1 = conn.createStatement();
                String sql1 = "Insert into customer_division_result (hour,weight) values ('" + Hour + "','"
                        + randcustomerweight + "')";
                System.out.println(sql1);
                stmt1.executeUpdate(sql1);

            }
            rs.close();
            stmt.close();

            java.sql.Statement stmt3 = conn.createStatement();
            String sql3 = "select sum(Weight) weight from customer_division_result;";
            System.out.println(sql3);
            java.sql.ResultSet rs3 = stmt3.executeQuery(sql3);


            if (rs3.next()) {
                System.out.println(rs3);
                totalweight = rs3.getInt("weight");
                System.out.println(totalweight);
            }
            rs3.close();
            stmt3.close();

            int custperweight = customercount / totalweight;

            java.sql.Statement stmt4 = conn.createStatement();
            String sql4;
            sql4 = "select * from Customer_division_result";
            java.sql.ResultSet rs4 = stmt4.executeQuery(sql4);
            while (rs4.next()) {
                int Hour = rs4.getInt("Hour");
                int Weight = rs4.getInt("Weight");
                int totalcustcount = Weight * custperweight;

                System.out.println(Hour + " " + Weight + " " + custperweight + " " + totalcustcount);

                java.sql.Statement stmt5 = conn.createStatement();
                String sql5 = "Update Customer_division_result set Custcount = " + totalcustcount + " where Hour = " + Hour;
                System.out.println(sql5);
                stmt5.executeUpdate(sql5);
            }
            rs4.close();
            stmt4.close();




            java.sql.Statement stmt5 = conn.createStatement();
            String sql5;
            sql5 = "select hour,Custcount from Customer_division_result";
            java.sql.ResultSet rs5 = stmt5.executeQuery(sql5);


            while (rs5.next()) {
                int Hour = rs5.getInt("Hour");
                int CustCount = rs5.getInt("Custcount");
                System.out.println(Hour);
                i=0;
                while (CustCount != i) {
                    try{
                        conn.setAutoCommit(false);
                    int k = 0;
                    while (CustCount != i && k != 10000) {
                        ++customernumber;
                        int customerorder = rand.nextInt(10) + 1;
                        String ordersort = null;
                        if (customerorder < 6) {
                            ordersort = "Chicken";
                        } else if (customerorder == 6 || customerorder == 7) {
                            ordersort = "Beef";
                        } else {
                            ordersort = "Pork";
                        }
                        // System.out.println("##DEBUG## | Hour = " + Hour + " Customernumber =  "+ customernumber +" Count is: " + i + ". The ordernumber is: " + customerorder
                        // 		+ ". The ordername = " + ordersort);

                        java.sql.Statement stmt6 = conn.createStatement();
                        String sql6 = "Insert into customer_orders (Hour,CustomerNumber,OrderNumber,OrderName) values ('" + Hour + "','" + customernumber + "','" + customerorder + "','" + ordersort + "');";
                        //System.out.println(sql6);
                        stmt6.executeUpdate(sql6);
                        //System.out.println(k + " " + i);

                        i++;
                        k++;
                    } conn.commit();
                    } catch (SQLException se){
                        conn.rollback();
                }

                }
            }

                rs5.close();
                stmt5.close();



            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");

    }

}

